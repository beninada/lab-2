package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(((this.getValue() * (float)9) / (float)5) + (float)32);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin(this.getValue() + (float)273);
	}
}
