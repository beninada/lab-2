/**
 * 
 */
package edu.ucsd.cs110w.temperature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * TODO (binada): write class javadoc
 *
 * @author binada
 */
public class TemperatureConverter {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String input = null;
		Temperature inputTemp = null, outputTemp = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			System.out.println("\nAvailable units: C, F, K");
			System.out.println("Enter temperature to convert (i.e. 36.8 C, 451 F): ");
			
			try {
				if((input = reader.readLine()) == null) System.exit(0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String[] temp_in = input.split(" ");
			
			float temp_val = Float.parseFloat(temp_in[0]);
			
			switch(temp_in[1].toLowerCase().charAt(0)) {
			case 'c':
				inputTemp = new Celsius(temp_val);
				break;
			case 'f':
				inputTemp = new Fahrenheit(temp_val);
				break;
			case 'k':
				inputTemp = new Kelvin(temp_val);
				break;
			default:
				System.out.println("Invalid entry!!\n\n");
				continue;
			}
			
			System.out.print("Enter the unit to convert TO: ");
			try {
				if((input = reader.readLine()) == null) System.exit(0);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch(input.toLowerCase().charAt(0))
			{
			case 'c':
				outputTemp = inputTemp.toCelsius();
				break;
			case 'f':
				outputTemp = inputTemp.toFahrenheit();
				break;
			case 'k':
				outputTemp = inputTemp.toKelvin();
				break;
			default:
				System.out.println("Invalid entry!!\n\n");
				continue;
			}
			
			System.out.println("\n The converted temperature is " + outputTemp.toString() + "\n\n");
		}
	}
}
