/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (binada): write class javadoc
 *
 * @author binada
 */
public class Kelvin extends Temperature {
	public Kelvin(float v) {
		super(v);
		// TODO Auto-generated constructor stub
	}

	public String toString()
	{
		return this.getValue() + " K";
	}
	
	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(this.getValue() - (float)273);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this.toCelsius().toFahrenheit();
	}

	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}

}
